#!/usr/bin/env node

/* eslint-disable no-console */

const express = require('express')
const morgan = require('morgan')
const path = require('path')
const favicon = require.resolve('../static/img/favicon.ico')
const ms = require('ms')

const argv = require('yargs')
  .option('port', {
    describe: 'port to bind on',
    default: 5000
  })
  .option('mediaDir', {
    describe: 'base directory containing the media files'
  })
  .option('cacheDir', {
    describe: 'base directory for the thumbnails'
  })
  .config('config', 'path to a config file', (file) => require(path.resolve(file)))
  .demandOption(['mediaDir', 'cacheDir'])
  .parse()

const app = express()

app.use(morgan('tiny'))

app.get('/favicon.ico', (req, res) => res.sendFile(favicon, {
  maxAge: ms('1 year'),
  immutable: true
}))
app.use('/backend', require('../src/index')(argv.mediaDir, argv.cacheDir))
app.use(require('../src/frontend')())

app.listen(argv.port, function () {
  console.log(`Gachou server is listening on port ${argv.port}!`)
})
