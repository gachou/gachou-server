/*!
 * gachou-server <https://github.com/gachou/gachou-server>
 *
 * Copyright (c) 2018 Nils Knappmeier.
 * Released under the MIT license.
 */

/* eslint-env mocha */

const gachouServer = require('../')
const chai = require('chai')
chai.use(require('dirty-chai'))
const expect = chai.expect

describe('gachou-server:', function () {
  it("should be executed", function () {
    expect(gachouServer()).to.equal('gachouServer')
  })
})
