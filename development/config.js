const path = require('path')

module.exports = {
  mediaDir: path.join(__dirname, 'media'),
  cacheDir: path.join(__dirname, 'cache'),
  port: 3000
}
