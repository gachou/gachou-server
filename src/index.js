/*!
 * gachou-server <https://github.com/gachou/gachou-server>
 *
 * Copyright (c) 2018 Nils Knappmeier.
 * Released under the MIT license.
 */
const express = require('express')
const {Scaler} = require('./scaler')
const store = require('./store')

module.exports = createRouter

/**
 *
 * Describe your module here
 * @public
 */
function createRouter (mediaDir, cacheDir) {
  const router = express.Router()
  const scaler = new Scaler(mediaDir, cacheDir)

  router.use('/store', store(mediaDir, cacheDir, scaler))
  router.use('/scaled/:spec/', wrap(createThumbnail))
  router.use('/original', express.static(mediaDir))

  /**
   * Creates a thumbnail, using smartcrop-sharp.
   * Size is extracted via the query-parameters "size=widthxheight"
   *
   * @param req
   * @param res
   * @param next
   * @returns {Promise.<*>}
   */
  async function createThumbnail (req, res, next) {
    const spec = req.params.spec
    const relativePath = req.path.slice(1) // remove trailing slash
    try {
      const thumbnailPath = await scaler.scale(relativePath, spec)
      return res.sendFile(thumbnailPath)
    } catch (err) {
      if (err.code === 'ENOENT') {
        return res.send(404, {code: 404, message: 'File not found'})
      }
      return res.send(500, {code: 500, message: 'Unkown error', err: err})
    }
  }

  return router
}

function wrap (fn) {
  return function (req, res, next) {
    fn(req, res, next).catch(next)
  }
}
