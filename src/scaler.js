const smartcrop = require('smartcrop-sharp')
const path = require('path')
const fs = require('fs')
const pify = require('pify')
const access = pify(fs.access)
const unlink = pify(fs.unlink)
const rename = pify(fs.rename)
const mkdirp = pify(require('mkdirp'))
const sharp = require('sharp')
const magic = require('./magic')
const spawn = require('child_process').spawn
const limit = require('p-limit')(2)

class Scaler {
  constructor (mediaDir, cacheDir) {
    this.mediaDir = mediaDir
    this.thumbsDir = path.resolve(cacheDir, 'thumbs')
    this.pendingScaleOperations = {}
  }

  /**
   *
   * @param {string} relativePath path to the source file relative to this.mediaDir
   * @param {string} spec a string specifying how large the target file should be (i.e. 100x100)
   * @return {Promise<string>} the path to the scaled file
   */
  async scale (relativePath, spec) {
    const originalFile = path.join(this.mediaDir, relativePath)
    // Make sure the file is readble
    await access(originalFile, fs.R_OK)
    const scaledFile = path.join(this.thumbsDir, spec, relativePath)
    try {
      // If the scaled version exists, return it immediately
      await access(scaledFile, fs.F_OK)
      return scaledFile
    } catch (err) {
      if (err.code !== 'ENOENT') {
        throw err
      }
    }
    // Ignore ENOENT
    try {
      // Make sure that the scaler is not invoked for a target-file, if there is already
      // a scaler running that computes this target
      if (!this.pendingScaleOperations[scaledFile]) {
        this.pendingScaleOperations[scaledFile] = doScale(originalFile, spec, scaledFile)
      }
      // Wait for the promise to resolve
      await this.pendingScaleOperations[scaledFile]
      return this.pendingScaleOperations[scaledFile]
    } finally {
      // This should happen, when the promise is resolved
      delete this.pendingScaleOperations[scaledFile]
    }
  }

  /**
   * Returns an object containing the components of a scale specification
   * @param {string} spec
   * @return {{width: number, height: number}}
   */
  static parseSpec (spec) {
    const match = spec.match(/(\d+)x(\d+)/)
    if (match) {
      return {
        width: Number(match[1]),
        height: Number(match[2])
      }
    }
    throw new Error('Could not parse scale specificiton: ' + spec)
  }
}

async function doScale (originalFile, spec, scaledFile) {
  // We need to create the thumbnail
  await mkdirp(path.dirname(scaledFile))

  const parsedSpec = Scaler.parseSpec(spec)

  let mimeType = await magic(originalFile)
  // Intermediate filename for atomic moves
  const intermediateFile = scaledFile + '.intermediate' + path.extname(scaledFile)
  switch (mimeType) {
    case 'image/jpeg':
    case 'image/png':
      await limit(() => thumbWithSharp(originalFile, parsedSpec, intermediateFile))
      break
    case 'video/mp4':
      await limit(() => videoThumbnail(originalFile, parsedSpec, intermediateFile))
      break
    case 'image/svg':
      // SVGs have no need for scaling
      return originalFile
    default:
      throw new Error('Unsupported mime-type:', mimeType)
  }
  await rename(intermediateFile, scaledFile)
  return scaledFile
}

/**
 * Uses sharp and smartcrop to scale the image
 * @param originalFile
 * @param spec
 * @param scaledFile
 */
async function thumbWithSharp (originalFile, spec, scaledFile) {
  // Crop and resize
  const result = await smartcrop.crop(originalFile, {width: spec.width, height: spec.height})
  const crop = result.topCrop
  await sharp(originalFile)
    .extract({width: crop.width, height: crop.height, left: crop.x, top: crop.y})
    .resize(spec.width, spec.height)
    .rotate()
    .toFile(scaledFile)
  return scaledFile
}

async function videoThumbnail (originalFile, spec, scaledFile) {
  const tmpFile = scaledFile + '.tmp' + path.extname(scaledFile)
  await ffmpeg(
    '-y',
    '-i', originalFile,
    '-vf', 'fps=5,setpts=0.1*PTS,' + // less images, faster playback
    `scale=w=${spec.width}:h=${spec.height}:force_original_aspect_ratio=increase,` +  // scale down to outer box
    `crop=${spec.width}:${spec.height}`, // crop box
    '-aspect', `${spec.width}:${spec.height}`,
    '-an', // mute audio
    tmpFile
  )
  // Stabilize image
  const transformFile = `${scaledFile}.trf`
  await ffmpeg('-y', '-i', tmpFile, '-vf', `vidstabdetect=result=${transformFile}`, scaledFile)
  await ffmpeg('-y', '-i', tmpFile, '-vf', `vidstabtransform=input=${transformFile}`, scaledFile)
  await Promise.all([unlink(tmpFile), unlink(transformFile)])
  return scaledFile
}

function ffmpeg (...args) {
  // Log command line as it would be entered on bash (JSON.stringify surrounds string with quotes and escapes quotes)
  console.log('Running: ffmpeg ', args.map(p => JSON.stringify(p)).join(' '))
  return new Promise((resolve, reject) => {
    const child = spawn('ffmpeg', args, {stdio: 'inherit'})
    child.on('exit', code => code === 0 ? resolve() : reject(new Error('ffmpeg exited with code ' + code)))
  })
}

module.exports = {
  Scaler
}
