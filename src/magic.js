const mmm = require('mmmagic')

const defaultMagic = require.resolve('mmmagic/magic/magic.mgc')
const additionalMagic = require.resolve('./magic/additionalMagic.mgc')
const magic = new mmm.Magic(`${defaultMagic}:${additionalMagic}`, mmm.MAGIC_MIME_TYPE)

/**
 * Promisified version of "mmm.Magic#detecFile" with mime-type output
 * @param {string} file path to the checked file.
 * @return {Promise<string>} the mime-type of the file
 */
module.exports = function magicP (file) {
  return new Promise((resolve, reject) => {
    magic.detectFile(file, (err, result) => err ? reject(err) : resolve(result))
  })
}
