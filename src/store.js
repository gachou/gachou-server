const stream = require('directory-tree-stream')
const express = require('express')
const path = require('path')
const _ = require('lodash')
const magic = require('./magic')
const Datastore = require('nedb')
const deep = require('deep-aplus')(Promise)
const mkdirp = require('pify')(require('mkdirp'))
const mapStream = require('map-stream-limit')

const DB_COMPACT_INTERVAL = 1000 * 60 * 60 * 12

/**
 *
 * @param mediaDir
 * @param cacheDir
 */
module.exports = function storeRouter (mediaDir, cacheDir, scaler) {
  const router = express.Router()
  Store.create(mediaDir, cacheDir).then(store => {
    router.post('/update', wrap(async function (req, res) {
      crawl()
      res.status(200).send({state: 'started'})
    }))

    router.get('/query', wrap(async function (req, res) {
      res.status(200).send(await store.find({
        year: req.query.year,
        month: req.query.month,
        mimeType: req.query.mimeType
      }, req.query.start, req.query.limit))
    }))

    function crawl () {
      return stream(mediaDir)
        .on('error', console.error)
        .pipe(mapStreamP(async (file) => {
          try {
            console.log('Found', file.path)
            if (file.stat.isFile()) {
              await scaler.scale(file.path, '300x300')
            }
            await store.add(file)
          } catch (err) {
            console.error(err, err.stack)
          }
          await new Promise((resolve) => setTimeout(resolve, Number(process.env.CRAWL_DELAY) || 100))
        }, 1))
        .on('end', function () {
          store.loadDatabases()
        })
        .on('error', console.error)
    }
  }).catch(console.error)

  return router
}

class Store {
  constructor (mediaDir, files, facets) {
    this.mediaDir = mediaDir
    this.files = files
    this.facets = facets
    setInterval(() => this.loadDatabases(), DB_COMPACT_INTERVAL)
  }

  static async create (mediaDir, cacheDir) {
    const dbDir = path.join(cacheDir, 'db')
    await mkdirp(dbDir)
    const files = new Datastore({filename: path.join(dbDir, 'files.json'), autoload: true})
    await asPromise((cb) => files.ensureIndex({fieldName: 'year', unique: false}, cb))
    await asPromise((cb) => files.ensureIndex({fieldName: 'month', unique: false}, cb))
    await asPromise((cb) => files.ensureIndex({fieldName: 'file', unique: true}, cb))
    await asPromise((cb) => files.ensureIndex({fieldName: 'mimeType', unique: false}, cb))
    const facets = new Datastore({filename: path.join(dbDir, 'facets.json'), autoload: true})
    return new Store(mediaDir, files, facets)
  }

  /**
   * Reload (and compress) the database files
   * @return {Promise.<void>}
   */
  async loadDatabases () {
    console.log('Compacting databases')
    this.files.persistence.compactDatafile()
    this.facets.persistence.compactDatafile()
  }

  async add (file) {
    const name = path.basename(file.path)
    return mapRegex(name, /(\d{4})-(\d{2})-.*/, async (match, year, month) => {
      const fullPath = path.join(this.mediaDir, file.path)
      let mimeType = await magic(fullPath)
      const entry = {
        year,
        month,
        mimeType,
        file: file.path,
        name: path.basename(file.path)
      }
      console.log('Adding ', entry)
      await asPromise(cb => this.files.update({file: file.path}, entry, {upsert: true}, cb))
      await asPromise(cb => this.facets.update({id: 'year', value: year}, {
        id: 'year',
        value: year
      }, {upsert: true}, cb))
      await asPromise(cb => this.facets.update({id: 'month', value: month}, {
        id: 'month',
        value: month
      }, {upsert: true}, cb))
      await asPromise(cb => this.facets.update({id: 'mimeType', value: mimeType}, {
        id: 'mimeType',
        value: mimeType
      }, {upsert: true}, cb))
    })
  }

  async find (query, start = 0, limit = 12) {
    const cleanQuery = _.pickBy(query, value => value) // pick truthy values
    const results = await asPromise(cb => this.files
      .find(cleanQuery)
      .sort({file: 1})
      .skip(start)
      .limit(limit)
      .exec(cb))
    return {
      facets: [
        await this.computeFacets(cleanQuery, 'year', 'Year'),
        await this.computeFacets(cleanQuery, 'month', 'Month'),
        await this.computeFacets(cleanQuery, 'mimeType', 'Type')
      ],
      context: {
        start,
        limit,
        total: await asPromise(cb => this.files.count(cleanQuery).exec(cb))
      },
      results
    }
  }

  async computeFacets (cleanQuery, id, name) {
    const allFacets = await asPromise(cb => this.facets.find({id}).sort({value: 1}).exec(cb))
    const values = await deep(allFacets.map(async (facetValue) => ({
      value: facetValue.value,
      active: facetValue.value === cleanQuery[id],
      count: await asPromise(cb => this.files.count({...cleanQuery, [id]: facetValue.value}, cb))
    })))
    return {
      id, name, values, active: true
    }
  }
}

function mapRegex (string, regex, callback) {
  const match = string.match(regex)
  if (match) {
    return callback.apply(this, match)
  } else {
    return undefined
  }
}

function asPromise (fn) {
  return new Promise((resolve, reject) => {
    fn((err, result) => err ? reject(err) : resolve(result))
  })
}

function wrap (fn) {
  return function (req, res, next) {
    fn(req, res, next).catch(next)
  }
}

/**
 * Wrapper for "map-stream-limit" to work with promise base functions
 * @param {function(*):Promise<*>} fn a function mapping an input value to the promise for an output
 * @param concurrency
 * @returns {stream}
 */
function mapStreamP (fn, concurrency) {
  let wrapper = (input, cb) => fn(input).then((output) => cb(null, output), (err) => cb(err))
  return mapStream(wrapper, concurrency)
}
