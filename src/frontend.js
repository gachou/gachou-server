const express = require('express')
const path = require('path')

module.exports = function () {
  const staticDir = path.resolve(__dirname, '..', 'static')
  const indexHtml = path.resolve(__dirname, '..', 'static', 'index.html')
  const router = new express.Router()
  router.use('/browse', (req, res) => res.sendFile(indexHtml))
  router.use('/', express.static(staticDir))
  return router
}
