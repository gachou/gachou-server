FROM node:8

# Download ffmpeg git build from https://johnvansickle.com/ffmpeg/
RUN wget https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-64bit-static.tar.xz && \
    mkdir -p /opt/ffmpeg && \
    tar --strip-components=1 -C /opt/ffmpeg/ -xf ffmpeg-git-64bit-static.tar.xz && \
    rm ffmpeg-git-64bit-static.tar.xz && \
    ln -s /opt/ffmpeg/ffmpeg /usr/local/bin/ffmpeg && \
    ln -s /opt/ffmpeg/ffmpeg-10bit /usr/local/bin/ffmpeg-10bit && \
    ln -s /opt/ffmpeg/ffprobe /usr/local/bin/ffprobe && \
    ln -s /opt/ffmpeg/qt-faststart /usr/local/bin/faststart
    
RUN apt-get update && apt-get -y install imagemagick

# From: https://gist.github.com/alkrauss48/2dd9f9d84ed6ebff9240ccfa49a80662
# Set the home directory to our app user's home.
ENV HOME=/home/gachou
ENV APP_HOME=/gachou/app
RUN mkdir -p $HOME && \
    mkdir -p $APP_HOME && \
    groupadd -r gachou && \
    useradd -r -g gachou -d ${HOME} -s /sbin/nologin -c "gachou user" gachou && \
    chown gachou.gachou $HOME

WORKDIR $APP_HOME

ADD ./package.json $APP_HOME
RUN cd $APP_HOME && npm install --production
ADD ./src $APP_HOME/src
ADD ./static $APP_HOME/static
ADD ./bin $APP_HOME/bin
ADD ./docker $APP_HOME/docker
ADD https://github.com/tianon/gosu/releases/download/1.10/gosu-amd64 /usr/local/bin/gosu
RUN chmod a+x /usr/local/bin/gosu

VOLUME /gachou/media
VOLUME /gachou/cache
EXPOSE 8080
ENTRYPOINT ["/gachou/app/docker/entrypoint.sh"]
CMD ["/usr/local/bin/node", "bin/server.js", "--config", "docker/config.js"]