This is a Proof-of-Concept, for the gachou-server. 
It has been built without testing and without architectural design.

See https://github.com/gachou/gachou-backend for the version that is currently developed
(but not ready yet)


# Running the POC

**Attention: There is almost no documentation yet, and the programs are
not stable. You can try to run this, but be aware of possible DATA LOSS 
and FRUSTRATION**

* Copy a couple of images to a different directory. 
  For files without CreationDate-Tags (e.g. "*.mts"), make sure to retain
  the last-modification-date of the files
* Use [normalize-videos](https://github.com/gachou/normalize-video)
  to convert videos to "mp4" (a format which can save XMP-tags)
* use the [group-by-date](https://github.com/gachou/group-by-date)
  project to create a date-based directory structure from the photos.

In order to run the POC, clone the project and add a `.env`-file in the 
project directory.


```
# DOMAIN_SUFFIX use by the traefik rules in the docker-compose file
# Not important if you don't use the https://traefik.io/ load-balancer
DOMAIN_SUFFIX=dev.knappi.org

# Directory containing the photo files. The format of the files must match "yyyy-mm-dd_hh-MM-ss-.*"
# Because the creation date is extracted from the filename
MEDIA_DIR=/home/nknappmeier/Desktop/Camera-sorted

# A (writable) directory to write thumbnails and the database file to
CACHE_DIR=/home/nknappmeier/projects/gachou/docker-cache

# Delay between crawling two files in order to keep the system responsive when
# the crawler is active
CRAWL_DELAY=50
```

The run `docker-compose up` and point your browser to `http://localhost:8080` 